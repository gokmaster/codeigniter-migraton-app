# Codeigniter Migrator
This CodeIgniter app allows you to run database migrations through the terminal.

## How to use this Migrator app?

* Change the database settings in **application/config/database.php** to match your database. An example is given below.

```
	'hostname' => 'localhost',
	'username' => 'root',
	'password' => '',
	'database' => 'mydbname',
	'dbdriver' => 'mysqli',
```

* If you need to change the migration type, open **application/config/migration.php** and set value of `$config['migration_type']` 
to either `'timestamp'` or `'sequential'`. 


* Place your migration files in **application/migrations** folder.


* To see a list of commands, `cd` into the root directory of this app, and type `php index.php migrate help` in terminal.

## Some commonly used commands

* Migrate to the very latest migration version.
```
	php index.php migrate latest
```
.............................................

* Migrate to the currently set migration version.
```
	php index.php migrate current
```
.............................................

* Rollback the migrations one step back.
```
	php index.php migrate last
```
.............................................

* Rollback to the '$version' version.
```
	php index.php migrate rollback($version = 0)
```
.............................................

* Deletes all migration files, Resets the migrations table and calls $this::rollback(0) if $rollback is set to TRUE.
```
	php index.php migrate reset($rollback = TRUE)
```
.............................................



<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<meta charset="UTF-8">

<!--optimise viewing on mobile devices-->
<meta name="viewport" content="width=device-width, initial-scale=0.9">

<!--requireJS library-->
<!-- <script data-main="js/reqJSconfig" src="js/lib/require.js"></script> -->


<!--jquery UI google CDN-->
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">

<link href="<?php echo base_url(); ?>res/styles/styles.css" rel="Stylesheet" type="text/css" />